<?php

// Define paths
define('EXAMPLE1_ROOT', dirname(__FILE__));

// Load resources
require EXAMPLE1_ROOT . '/../bootstrap.php';
require EXAMPLE1_ROOT . '/Application.php';
require EXAMPLE1_ROOT . '/SleepingWorker.php';

// Load config
$configManager = &get_config_manager(EXAMPLE1_ROOT . '/config.ini');

$config = &get_config($configManager);
$config->workers->classLoaderFile = realpath(EXAMPLE1_ROOT . '/worker_class_loader.php');

// Create a logger
$logger = &get_logger($configManager);

// Bootstrap the application
$loopFactory = &new EventLoopFactory;
$loop = &$loopFactory->create(&$config, &$logger);
$app = &new Application($loop);

// Run it
$loop->run();

// Done
exit(0);
