<?php

class SleepingWorker extends WorkerObject
{
    /**
     * Do something that takes a while
     *
     * @param int $usecs
     * @return int
     */
    function run($usecs)
    {
        usleep(floor($usecs / 2));
        $this->logger->debug('Sleeping Worker', 'Debug');
        $this->logger->info('Sleeping Worker', 'Info');
        $this->logger->warn('Sleeping Worker', 'Warn');
        $this->logger->error('Sleeping Worker', 'Error');
        usleep(floor($usecs / 2));
        return $usecs;
    }
}
