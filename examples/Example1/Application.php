<?php

class Application
{
    var $loop;

    var $heartbeatId;

    var $socket;

    /** @noinspection PhpInconsistentReturnPointsInspection */
    /**
     * @param EventLoop $loop
     * @return Application
     */
    function Application(&$loop)
    {
        $this->loop = &$loop;

        $this->heartbeatId = $loop->every(1000000, array(&$this, 'heartbeat'));
        $job = &$loop->async('SleepingWorker::run', 10000000);

        $job->on('begin', array(&$this, 'jobBegin'));
        $job->on('complete', array(&$this, 'jobComplete'));
        $job->on('error', array(&$this, 'jobError'));

        foreach (array('google.com', 'google.co.uk', 'stackoverflow.com', 'foo.com', 'daverandom.com') as $domain) {
            $loop->resolveHost($domain, array(&$this, 'dnsCallback'), true);
        }

        $this->socket = &$loop->openStream('tcp://google.com:80');
        $this->socket->on('connect', array(&$this, 'socketConnect'));
        $this->socket->on('data', array(&$this, 'socketData'));
        $this->socket->on('error', array(&$this, 'socketError'));

        $ldap = &new LDAPConnection($loop, 'ldap://127.0.0.1:4000', 'INTERNAL', '112233445566');
        $ldap->search(array(&$this, 'ldapSearch'), 'cn=Users', 'objectClass=User');
    }

    function ldapSearch($result, $error = null)
    {
        echo "LDAP search results: " . ($result ? count($result) : 'Failed: ' . $error) . "\n";
    }

    function socketConnect()
    {
        echo "SocketStream is connected!\n";
        $this->socket->write("GET / HTTP/1.0\r\nHost: google.com\r\n\r\n");
    }

    function socketData($data)
    {
        echo "SocketStream got data: $data\n";
    }

    function socketError($message)
    {
        echo "SocketStream got error: {$message}\n";
    }

    function initShutdown()
    {
        echo "Shutting down in 7 seconds\n";
        $this->loop->in(7000000, array(&$this, 'shutdown'), 'hello!');
    }

    function shutdown($message)
    {
        echo "Shutting down with message {$message}\n";
        $this->loop->cancel($this->heartbeatId);
    }

    function dnsCallback($host, $address, $err = null)
    {
        if ($address === null) {
            echo "DNS lookup ($host) failed: $err\n";
        } else {
            echo "DNS lookup ($host) success: $address\n";
        }
    }

    function heartbeat()
    {
        echo "Tick\n";
    }

    function jobBegin()
    {
        echo "Job begin\n";
    }

    function jobComplete()
    {
        echo "Job complete\n";
        $this->initShutdown();
    }

    function jobError($message)
    {
        echo "Job error: {$message}\n";
        $this->initShutdown();
    }
}
