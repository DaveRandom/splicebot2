<?php

define('EXAMPLE_LOGLEVEL_ERROR', 10);
define('EXAMPLE_LOGLEVEL_WARN',  20);
define('EXAMPLE_LOGLEVEL_INFO',  30);
define('EXAMPLE_LOGLEVEL_DEBUG', 40);

class ExampleLogger
{
    /**
     * @var resource
     */
    var $handle;

    /**
     * @var int $level
     */
    var $level;

    /**
     * @var string
     */
    var $format = '[%s] %-12s | %-5s | %s: %s';

    /**
     * @var string[]
     */
    var $levelStrs = array(
        EXAMPLE_LOGLEVEL_ERROR => 'ERROR',
        EXAMPLE_LOGLEVEL_WARN  => 'WARN',
        EXAMPLE_LOGLEVEL_INFO  => 'INFO',
        EXAMPLE_LOGLEVEL_DEBUG => 'DEBUG',
    );

    /** @noinspection PhpInconsistentReturnPointsInspection */
    /**
     * Constructor
     *
     * @param resource $handle
     * @param int $level
     * @return ExampleLogger
     */
    function ExampleLogger($handle, $level = EXAMPLE_LOGLEVEL_INFO)
    {
        $this->handle = $handle;
        $this->level = $level;
    }

    /**
     * Write a message to the output handle
     *
     * @param float $timestamp
     * @param string $level
     * @param string $message
     * @param string $source
     * @param string $process
     */
    function _output($level, $timestamp, $message, $source, $process)
    {
        if ($this->level >= $level) {
            fwrite($this->handle, sprintf($this->format, $timestamp, $process, $this->levelStrs[$level], $source, $message) . "\n");
        }
    }

    /**
     * Get the string representation of a log level constant
     *
     * @param float $timestamp
     * @return string
     */
    function _getTimeStr($timestamp)
    {
        $secs = floor($timestamp);
        return sprintf('%s.%03s', date('Y-m-d H:i:s', $secs), floor(($timestamp - $secs) * 1000));
    }

    /**
     * Output an error-level log message
     *
     * @param float $timestamp
     * @param string $message
     * @param string $source
     * @param string $process
     */
    function error($timestamp, $message, $source, $process)
    {
        $this->_output(EXAMPLE_LOGLEVEL_ERROR, $this->_getTimeStr($timestamp), $message, $source, $process);
    }

    /**
     * Output a warn-level log message
     *
     * @param float $timestamp
     * @param string $message
     * @param string $source
     * @param string $process
     */
    function warn($timestamp, $message, $source, $process)
    {
        $this->_output(EXAMPLE_LOGLEVEL_WARN, $this->_getTimeStr($timestamp), $message, $source, $process);
    }

    /**
     * Output an info-level log message
     *
     * @param float $timestamp
     * @param string $message
     * @param string $source
     * @param string $process
     */
    function info($timestamp, $message, $source, $process)
    {
        $this->_output(EXAMPLE_LOGLEVEL_INFO, $this->_getTimeStr($timestamp), $message, $source, $process);
    }

    /**
     * Output a debug-level log message
     *
     * @param float $timestamp
     * @param string $message
     * @param string $source
     * @param string $process
     */
    function debug($timestamp, $message, $source, $process)
    {
        $this->_output(EXAMPLE_LOGLEVEL_DEBUG, $this->_getTimeStr($timestamp), $message, $source, $process);
    }
}
