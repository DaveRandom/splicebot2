<?php

// Examples should show all errors (to show that there aren't any)
error_reporting(~0);

/**
 * Get a config manager with the base config and optionally a file
 *
 * @param string $file
 * @return ExampleConfigManager
 */
function &get_config_manager($file = null)
{
    $configManager = &new ExampleConfigManager;

    $configManager->load(EXAMPLES_ROOT . '/config.default.ini');
    if ($file !== null) {
        $configManager->load($file);
    }

    return $configManager;
}

/**
 * Get a config object from a config manager
 *
 * @param ExampleConfigManager $configManager
 * @return EventLoopConfig
 */
function &get_config($configManager)
{
    $config = &new EventLoopConfig;

    $config->sockets->connectTimeout = $configManager->get('sockets.client_connect_timeout');

    $config->workers->maxWorkers = $configManager->get('workers.max_dynamic_workers');
    $config->workers->maxIdleTime = $configManager->get('workers.max_idle_time');
    $config->workers->maxJobsPerProcess = $configManager->get('workers.max_jobs_per_process');
    $config->workers->objectCacheTTL = $configManager->get('workers.object_cache_ttl');
    $config->workers->phpBinaryPath = readlink('/proc/self/exe');

    $config->dns->maxIdleTime = $configManager->get('dns.worker_max_idle_time');
    $config->dns->lookupMode = strtolower($configManager->get('dns.lookup_mode')) === 'multi' ? DNS_LOOKUP_MODE_MULTI : DNS_LOOKUP_MODE_SINGLE;

    return $config;
}

/**
 * Get a logger object from a config manager
 *
 * @param ExampleConfigManager $configManager
 * @return ExampleLogger
 */
function &get_logger($configManager)
{
    $location = $configManager->get('logging.output_location');
    switch (strtolower($location)) {
        case 'stdout':
            $outputStream = STDOUT;
            break;

        default:
            if ((is_file($location) && is_writable($location)) || (is_dir(dirname($location)) && is_writable(dirname($location)))) {
                if ($fp = fopen($location, 'a')) {
                    $outputStream = $fp;
                    break;
                }
            }

        case 'stderr':
            $outputStream = STDERR;
            break;
    }

    $loggingLevel = array_search(strtoupper($configManager->get('logging.level')), array(
        EXAMPLE_LOGLEVEL_ERROR => 'ERROR',
        EXAMPLE_LOGLEVEL_WARN  => 'WARN',
        EXAMPLE_LOGLEVEL_INFO  => 'INFO',
        EXAMPLE_LOGLEVEL_DEBUG => 'DEBUG',
    ));
    if (!$loggingLevel) {
        $loggingLevel = EXAMPLE_LOGLEVEL_INFO;
    }

    return new ExampleLogger($outputStream, $loggingLevel);
}

// Define paths
define('EXAMPLES_ROOT', dirname(__FILE__));
define('EVENTLOOP_ROOT', EXAMPLES_ROOT . '/../src');

// Load lib and shared classes
require EVENTLOOP_ROOT . '/class_loader.php';
require EXAMPLES_ROOT . '/ExampleConfigManager.php';
require EXAMPLES_ROOT . '/ExampleLogger.php';
