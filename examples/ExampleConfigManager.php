<?php

class ExampleConfigManager
{
    /**
     * @var array
     */
    var $config = array();

    /** @noinspection PhpInconsistentReturnPointsInspection */
    /**
     * Constructor
     *
     * @param array $config
     * @return ExampleConfigManager
     */
    function ExampleConfigManager($config = array())
    {
        $this->config = (array) $config;
    }

    /**
     * Load config from a file
     *
     * @param string $file
     * @return ExampleConfigManager|bool
     */
    function &load($file)
    {
        if (!is_file($file) || !is_readable($file) || false === $config = parse_ini_file($file)) {
            return false;
        }

        if (isset($this)) {
            $this->config = array_merge($this->config, $config);
            return true;
        } else {
            return new ExampleConfigManager($config);
        }
    }

    /**
     * Get the value of an option
     *
     * @param string $key
     * @return mixed
     */
    function get($key)
    {
        return isset($this->config[$key]) ? $this->config[$key] : null;
    }

    /**
     * Set the value of an option
     *
     * @param string $key
     * @param mixed $value
     */
    function set($key, $value)
    {
        $this->config[$key] = $value;
    }
}
