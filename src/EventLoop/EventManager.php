<?php

class EventManager
{
    /**
     * @var array
     */
    var $_eventHandlers = array();

    /**
     * Trigger an event
     *
     * @param string $eventName
     * @internal param mixed ...$args
     */
    function trigger($eventName)
    {
        if (isset($this->_eventHandlers[$eventName])) {
            $args = func_get_args();
            $args = array_slice($args, 1);

            foreach ($this->_eventHandlers[$eventName] as $callbackInfo) {
                $handlerArgs = $callbackInfo[1] ? array_merge($callbackInfo[1], $args) : $args;

                if (call_user_func_array($callbackInfo[0], $handlerArgs) === false) {
                    break;
                }
            }
        }

        if (isset($this->_eventHandlers['*'])) {
            if (isset($args)) {
                array_unshift($args, $eventName);
            } else {
                $args = func_get_args();
                $args = array_merge(array($eventName), array_slice($args, 1));
            }

            foreach ($this->_eventHandlers['*'] as $callbackInfo) {
                $handlerArgs = $callbackInfo[1] ? array_merge($callbackInfo[1], $args) : $args;

                call_user_func_array($callbackInfo[0], $handlerArgs);
            }
        }
    }

    /**
     * Register a handler for an event
     *
     * @param string $eventName
     * @param callable $callback
     * @param array $args
     * @return bool
     */
    function on($eventName, $callback, $args = array())
    {
        if (!is_callable($callback)) {
            return false;
        }

        if (!isset($this->_eventHandlers[$eventName])) {
            $this->_eventHandlers[$eventName] = array();
        }
        $this->_eventHandlers[$eventName][] = array($callback, $args);

        return true;
    }

    /**
     * Unregister a handler for an event
     *
     * Returns the number of handlers removed
     *
     * @param string $eventName
     * @param callable $callback
     * @return int
     */
    function off($eventName, $callback = null)
    {
        $result = 0;

        if (isset($this->_eventHandlers[$eventName])) {
            if (!isset($callback)) {
                $result = count($this->_eventHandlers[$eventName]);
                unset($this->_eventHandlers[$eventName]);
            } else {
                foreach ($this->_eventHandlers[$eventName] as $i => $registeredCallback) {
                    if ($callback === $registeredCallback) {
                        $result++;
                        array_splice($this->_eventHandlers[$eventName], $i, 1);
                    }
                }
            }
        }

        return $result;
    }
}
