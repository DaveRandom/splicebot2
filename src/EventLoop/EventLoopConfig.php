<?php

class EventLoopConfig
{
    /**
     * Worker-specific configuration
     *
     * @var WorkerConfig
     */
    var $workers;

    /**
     * SocketStream-specific configuration
     *
     * @var SocketConfig
     */
    var $sockets;

    /**
     * DNS-specific configuration
     *
     * @var DNSConfig
     */
    var $dns;

    /**
     * Constructor
     */
    function EventLoopConfig()
    {
        $this->workers = new WorkerConfig;
        $this->sockets = new SocketConfig;
        $this->dns = new DNSConfig;
    }
}
