<?php

class ScheduleItemFactory
{
    /**
     * Create a new ScheduleItem
     *
     * @param int $id
     * @param float $timestamp
     * @param callable $callback
     * @param array $args
     * @param float $repeat
     * @return ScheduleItem
     */
    function create($id, $timestamp, $callback, $args, $repeat)
    {
        return new ScheduleItem($id, $timestamp, $callback, $args, $repeat);
    }
}
