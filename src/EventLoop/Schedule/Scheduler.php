<?php

/**
 * Shim to emulate PHP5's PHP_INT_MAX constant
 */
if (!defined('PHP_INT_MAX')) {
    for ($intMax = 0; is_int($intMax + $intMax + 1); $intMax += $intMax + 1);
    define('PHP_INT_MAX', $intMax);
}

class Scheduler
{
    /**
     * @var ScheduleItemFactory
     * @access private
     */
    var $_scheduleItemFactory;

    /**
     * @var EventLoopLogger
     * @access private
     */
    var $_logger;

    /**
     * @var ScheduleItem[]
     * @access private
     */
    var $_items = array();

    /**
     * @var int
     * @access private
     */
    var $_itemIdCounter = 0;

    /** @noinspection PhpInconsistentReturnPointsInspection */
    /**
     * Constructor
     *
     * @param ScheduleItemFactory $scheduleItemFactory
     * @param EventLoopLogger $logger
     * @return Scheduler
     */
    function Scheduler(&$scheduleItemFactory, &$logger)
    {
        $this->_scheduleItemFactory = &$scheduleItemFactory;
        $this->_logger = &$logger;
    }

    /**
     * Get the current time as a float
     *
     * @return float
     * @access private
     */
    function _now()
    {
        list($usec, $sec) = explode(' ', microtime());
        return ((float)$usec + (float)$sec);
    }

    /**
     * Get the next available item ID
     *
     * @return int
     * @access private
     */
    function _getNextItemId()
    {
        $id = $this->_itemIdCounter++;

        if ($this->_itemIdCounter >= PHP_INT_MAX) {
            $this->_itemIdCounter = PHP_INT_MAX;
        }

        return $id;
    }

    /**
     * Reschedule a recurring item
     *
     * @param ScheduleItem $item
     * @access private
     */
    function _repeatItem($item)
    {
        $timestamp = $item->timestamp;
        while ($timestamp <= $this->_now()) {
            $timestamp += $item->repeat;
        }

        $this->_logger->debug(null, 'Rescheduling item #%d', $item->id);

        $this->createItem($timestamp, $item->callback, $item->args, $item->repeat, $item->id);
    }

    /**
     * Execute all schedule items where the scheduled timestamp is in the past
     */
    function processActivity()
    {
        $now = $this->_now();

        while (($item = end($this->_items)) && $item->timestamp <= $now) {
            $item = array_pop($this->_items);
            $this->_logger->debug(null, 'Processing scheduled item #%d', $item->id);

            call_user_func_array($item->callback, $item->args);

            if ($item->repeat) {
                $this->_repeatItem($item);
            } else {
                $this->_logger->info(null, 'Schedule item #%d completed', $item->id);
            }
        }
    }

    /**
     * Inspect whether there is work in progress
     *
     * @return bool
     */
    function isActive()
    {
        $this->_logger->debug(null, 'Scheduled items: %d', count($this->_items));

        return (bool) $this->_items;
    }

    /**
     * Get the time until there is something to do
     *
     * @return float|null
     */
    function getNextActivityTime()
    {
        if (!$this->_items) {
            return null;
        }

        return $this->_items[count($this->_items) - 1]->timestamp - $this->_now();
    }

    /**
     * Add an item to the schedule
     *
     * @param float $timestamp
     * @param callable $callback
     * @param array $args
     * @param float $repeat
     * @param int $id
     * @return int
     */
    function createItem($timestamp, $callback, $args, $repeat = null, $id = null)
    {
        if (!is_callable($callback)) {
            $this->_logger->error(null, 'An attempt was made to schedule something that is not callable');
            return false;
        }

        $item = $this->_scheduleItemFactory->create(
            $id !== null ? $id : $this->_getNextItemId(),
            $timestamp, $callback, $args, $repeat
        );

        if ($this->_items) {
            $added = false;

            for ($i = count($this->_items) - 1; $i >= 0; $i--) {
                if ($this->_items[$i]->timestamp > $timestamp) {
                    array_splice($this->_items, $i + 1, 0, array($item));
                    $added = true;
                    break;
                }
            }

            if (!$added) {
                array_unshift($this->_items, $item);
            }
        } else {
            array_push($this->_items, $item);
        }

        $this->_logger->info(
            null, '%s schedule item #%d, timestamp: %f, repeating: %s',
            $id === null ? 'Created' : 'Rescheduled',
            $item->id, $timestamp, $repeat ? $repeat : 'no'
        );

        return $item->id;
    }

    /**
     * Remove an item from the schedule
     *
     * @param int $id
     * @return bool
     */
    function removeItem($id)
    {
        for ($i = 0, $l = count($this->_items); $i < $l; $i++) {
            if ($this->_items[$i]->id === $id) {
                array_splice($this->_items, $i, 1);
                $this->_logger->info(null, 'Removed item #%d from the schedule', $id);

                return true;
            }
        }

        return false;
    }

    /**
     * Destroy all existing schedule items
     */
    function shutdown()
    {
        while ($this->_items) {
            $this->removeItem($this->_items[0]->id);
        }
    }
}
