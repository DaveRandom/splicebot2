<?php

class SchedulerFactory
{
    /**
     * Create a new Scheduler
     *
     * @param EventLoopLogger $logger
     * @return Scheduler
     */
    function &create(&$logger)
    {
        return new Scheduler(new ScheduleItemFactory, $logger);
    }
}
