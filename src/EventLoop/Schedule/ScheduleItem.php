<?php

class ScheduleItem
{
    /**
     * @var int
     */
    var $id;

    /**
     * @var float
     */
    var $timestamp;

    /**
     * @var callable
     */
    var $callback;

    /**
     * @var array
     */
    var $args;

    /**
     * @var float
     */
    var $repeat;

    /** @noinspection PhpInconsistentReturnPointsInspection */
    /**
     * Constructor
     *
     * @param int $id
     * @param float $timestamp
     * @param callable $callback
     * @param array $args
     * @param float $repeat
     * @return ScheduleItem
     */
    function ScheduleItem($id, $timestamp, $callback, $args, $repeat)
    {
        $this->id = $id;
        $this->timestamp = $timestamp;
        $this->callback = $callback;
        $this->args = $args;
        $this->repeat = $repeat;
    }
}
