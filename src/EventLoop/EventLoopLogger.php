<?php

define('REACTOR_LOGLEVEL_ERROR', 10);
define('REACTOR_LOGLEVEL_WARN',  20);
define('REACTOR_LOGLEVEL_INFO',  30);
define('REACTOR_LOGLEVEL_DEBUG', 40);

class EventLoopLogger
{
    /**
     * @var object
     */
    var $_logger = null;

    /** @noinspection PhpInconsistentReturnPointsInspection */
    /**
     * Constructor
     *
     * @param object $logger
     * @return EventLoopLogger
     */
    function EventLoopLogger(&$logger)
    {
        if ($logger === null) {
            return $this;
        }

        if (!is_object($logger)) {
            echo "Logger is not an object\n";
            exit(1);
        }

        foreach (array('error', 'warn', 'info', 'debug') as $method) {
            if (!method_exists($logger, $method)) {
                printf("Logger does not implement the %s method\n", $method);
                exit(1);
            }
        }

        $this->_logger = &$logger;
    }

    /**
     * Get the current time as a float
     *
     * @return float
     * @access private
     */
    function _now()
    {
        list($usec, $sec) = explode(' ', microtime());
        return ((float)$usec + (float)$sec);
    }

    /**
     * Log an error-level message
     *
     * @param string $source
     * @param string $message
     * @internal param mixed ...$args
     */
    function error($source, $message)
    {
        if ($this->_logger !== null) {
            if (func_num_args() > 2) {
                $args = func_get_args();
                $message = call_user_func_array('sprintf', array_slice($args, 1));
            }

            $this->_logger->error($this->_now(), $message, $source !== null ? $source : 'event loop', 'main');
        }
    }

    /**
     * Log a warn-level message
     *
     * @param string $source
     * @param string $message
     * @internal param mixed ...$args
     */
    function warn($source, $message)
    {
        if ($this->_logger !== null) {
            if (func_num_args() > 2) {
                $args = func_get_args();
                $message = call_user_func_array('sprintf', array_slice($args, 1));
            }

            $this->_logger->warn($this->_now(), $message, $source !== null ? $source : 'event loop', 'main');
        }
    }

    /**
     * Log an info-level message
     *
     * @param string $source
     * @param string $message
     * @internal param mixed ...$args
     */
    function info($source, $message)
    {
        if ($this->_logger !== null) {
            if (func_num_args() > 2) {
                $args = func_get_args();
                $message = call_user_func_array('sprintf', array_slice($args, 1));
            }

            $this->_logger->info($this->_now(), $message, $source !== null ? $source : 'event loop', 'main');
        }
    }

    /**
     * Log a debug-level message
     *
     * @param string $source
     * @param string $message
     * @internal param mixed ...$args
     */
    function debug($source, $message)
    {
        if ($this->_logger !== null) {
            if (func_num_args() > 2) {
                $args = func_get_args();
                $message = call_user_func_array('sprintf', array_slice($args, 1));
            }

            $backtrace = debug_backtrace();
            $caller =  isset($backtrace[1]['class'])
                ? $backtrace[1]['class'] . $backtrace[1]['type'] . $backtrace[1]['function']
                : $backtrace[1]['function'];

            $this->_logger->debug($this->_now(), $caller . ': ' . $message, $source !== null ? $source : 'event loop', 'main');
        }
    }

    /**
     * Relay a log message with all fields populated
     *
     * @param string $level
     * @param float $timestamp
     * @param string $process
     * @param string $source
     * @param string $message
     */
    function relay($level, $timestamp, $source, $message, $process)
    {
        if ($this->_logger !== null) {
            call_user_func(array(&$this->_logger, $level), $timestamp, $message, $source, $process);
        }
    }
}
