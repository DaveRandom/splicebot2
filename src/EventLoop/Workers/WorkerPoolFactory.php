<?php

class WorkerPoolFactory
{
    /**
     * Create a new WorkerPool
     *
     * @param EventLoop $eventLoop
     * @param WorkerConfig $config
     * @param EventLoopLogger $logger
     * @param string $name
     * @return WorkerPool
     */
    function &create(&$eventLoop, &$config, &$logger, $name = 'Unnamed')
    {
        return new WorkerPool($eventLoop, $config, $logger, $name);
    }
}
