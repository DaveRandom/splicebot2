<?php

if (!class_exists('WorkerRequest')) {
    class WorkerRequest
    {
        /**
         * @var string
         */
        var $method;

        /**
         * @var array
         */
        var $args;

        /** @noinspection PhpInconsistentReturnPointsInspection */
        /**
         * Constructor
         *
         * @param string $method
         * @param array $args
         * @return WorkerRequest
         */
        function WorkerRequest($method, $args)
        {
            $this->method = $method;
            $this->args = $args;
        }
    }
}
