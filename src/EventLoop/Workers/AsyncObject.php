<?php

class AsyncObject
{
    /**
     * @var EventLoop
     */
    var $_eventLoop;

    /**
     * @var WorkerPool
     */
    var $_workerPool;

    /**
     * @var EventLoopLogger
     */
    var $_logger;

    /**
     * @var string
     */
    var $_className;

    /** @noinspection PhpInconsistentReturnPointsInspection */
    /**
     * Constructor
     *
     * @param EventLoop $eventLoop
     * @param WorkerPool $workerPool
     * @param EventLoopLogger $logger
     * @param string $className
     * @return AsyncObject
     */
    function AsyncObject(&$eventLoop, &$workerPool, &$logger, $className)
    {
        $this->_eventLoop = &$eventLoop;
        $this->_workerPool = &$workerPool;
        $this->_logger = &$logger;
        $this->_className = $className;
    }

    /**
     * Handle the error event on an async job
     *
     * @param AsyncJob $job
     * @param callable $callback
     * @param string $errStr
     */
    function _errorHandler(&$job, &$callback, $errStr)
    {
        $this->_logger->warn('Async object (' . $this->_className . ')', 'Caught error from AsyncJob: %s', $errStr);
        $job->off('error', array(&$this, '_errorHandler'));

        call_user_func($callback, false, $errStr);
    }

    /**
     * Call a method on the async object
     *
     * @param string $methodName
     * @param array $args
     * @param callable $callback
     * @return AsyncJob
     */
    function &call($methodName, $args, $callback)
    {
        $method = $this->_className . '->' . $methodName;
        $job = &new AsyncJob($method, $args);
        $this->_workerPool->pushJob($job);

        $this->_logger->info('Async object (' . $this->_className . ')', 'Queued method call, method: %s', $method);
        $job->on('complete', $callback);
        $job->on('error', array(&$this, '_errorHandler'), array(&$job, &$callback));

        return $job;
    }

    /**
     * Destroy this object and its worker pool
     */
    function destroy()
    {
        $this->_logger->info('Async object (' . $this->_className . ')', 'Destroying object');
        $this->_eventLoop->_removeObjectWorkerPool($this->_workerPool);
    }
}
