<?php

if (!class_exists('WorkerObject')) {
    class WorkerObject
    {
        /**
         * @var WorkerLogger
         */
        var $logger;

        /**
         * @var WorkerLogger
         */
        var $config;

        /** @noinspection PhpInconsistentReturnPointsInspection */
        /**
         * Constructor
         *
         * @param WorkerLogger $logger
         * @param WorkerConfig $config
         * @return WorkerObject
         */
        function WorkerObject(&$logger, &$config)
        {
            $this->logger = &$logger;
            $this->config = &$config;
        }
    }
}
