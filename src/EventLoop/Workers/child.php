<?php

// Load system classes
require dirname(__FILE__) . '/WorkerChildEndpoint.php';
require dirname(__FILE__) . '/WorkerObject.php';
require dirname(__FILE__) . '/WorkerConfig.php';
require dirname(__FILE__) . '/WorkerLogger.php';
require dirname(__FILE__) . '/WorkerRequest.php';
require dirname(__FILE__) . '/WorkerResponse.php';

// Bootstrap the worker application
stream_set_blocking(STDIN, 0);
stream_set_blocking(STDOUT, 0);

$endpoint = &new WorkerChildEndpoint(new WorkerLogger, STDIN, STDOUT);

// Run it
$endpoint->run();

// All done, exit with a success code
exit(WORKER_ERR_SUCCESS);
