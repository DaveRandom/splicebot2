<?php

if (!class_exists('WorkerResponse')) {
    define('WORKER_ERR_SUCCESS', 0);
    define('WORKER_ERR_FATAL', 64);

    // RECOVERABLE (code & 0x40) == 0

    // 11-20: Job init errors
    define('WORKER_ERR_INVALID_METHOD_SPEC', 11);
    define('WORKER_ERR_UNKNOWN_CLASS',       12);
    define('WORKER_ERR_UNKNOWN_METHOD',      13);

    // FATAL (code & 0x40) != 0

    // 65-90: Init errors
    define('WORKER_ERR_GETPID_FAILURE',            65);
    define('WORKER_ERR_CLASS_LOADER_NOT_DEFINED',  66);
    define('WORKER_ERR_CLASS_LOADER_NOT_EXIST',    67);
    define('WORKER_ERR_CLASS_LOADER_NOT_FILE',     68);
    define('WORKER_ERR_CLASS_LOADER_NOT_READABLE', 69);
    define('WORKER_ERR_INVALID_CONFIG_OBJECT',     70);

    // 91-100: I/O errors
    define('WORKER_ERR_WRITE_FAILURE',    91);
    define('WORKER_ERR_READ_FAILURE',     92);
    define('WORKER_ERR_READ_PIPE_CLOSED', 93);

    // 101-120: IPC protocol errors
    define('WORKER_ERR_UNKNOWN_COMMAND',    101);
    define('WORKER_ERR_UNKNOWN_JOB_FORMAT', 102);

    define('WORKER_RESPONSE_FLAG_LAST_ELEMENT', 1);
    define('WORKER_RESPONSE_FLAG_ARRAY_ELEMENT', 2);

    class WorkerResponse
    {
        /**
         * The response code
         *
         * @var int
         */
        var $code;

        /**
         * Flags associated with the response
         *
         * @var int
         */
        var $flags = 0;

        /**
         * The data payload
         *
         * @var mixed
         */
        var $data;

        /** @noinspection PhpInconsistentReturnPointsInspection */
        /**
         * Constructor
         *
         * @param int $code
         * @param mixed $data
         * @param int $flags
         * @return WorkerResponse
         */
        function WorkerResponse($code, $data, $flags = 0)
        {
            $this->code = $code;
            $this->data = $data;
            $this->flags = $flags;
        }
    }
}
