<?php

if (!class_exists('WorkerChildEndpoint')) {
    class WorkerChildEndpoint
    {
        /**
         * @var WorkerConfig
         */
        var $_config;

        /**
         * @var WorkerLogger
         */
        var $_logger;

        /**
         * @var resource
         */
        var $stdIn;

        /**
         * @var resource
         */
        var $stdOut;

        /**
         * @var resource[]
         */
        var $readStreams = array();

        /**
         * @var resource[]
         */
        var $writeStreams = array();

        /**
         * @var bool
         */
        var $running = false;

        /**
         * @var array
         */
        var $_writeBuffer = array();

        /**
         * @var string
         */
        var $_readBuffer = '';

        /**
         * @var object[]
         */
        var $_objectCache = array();

        /**
         * @var float[]
         */
        var $_objectCacheUsageHistory = array();

        var $_activeStream;

        /** @noinspection PhpInconsistentReturnPointsInspection */
        /**
         * Constructor
         *
         * @param WorkerLogger $logger
         * @param resource $stdIn
         * @param resource $stdOut
         * @return WorkerChildEndpoint
         */
        function WorkerChildEndpoint(&$logger, $stdIn, $stdOut)
        {
            $this->_logger = &$logger;
            $this->stdIn = $stdIn;
            $this->stdOut = $stdOut;
            $this->readStreams[] = $stdIn;
        }

        /**
         * Get the current time as a float
         *
         * @return float
         * @access private
         */
        function _now()
        {
            list($usec, $sec) = explode(' ', microtime());
            return ((float)$usec + (float)$sec);
        }

        /**
         * Get an instance of a class
         *
         * @param string $className
         * @return object
         */
        function &_getObject($className)
        {
            if (!isset($this->_objectCache[$className])) {
                $this->_logger->debug(null, 'Creating new object instance: %s', $className);
                $this->_objectCache[$className] = &new $className($this->_logger, $this->_config);
            } else {
                $this->_logger->debug(null, 'Reusing cached object instance: %s', $className);
            }

            return $this->_objectCache[$className];
        }

        /**
         * Mark an object in the cache as recently used
         *
         * @param $className
         */
        function _updateObjectCacheUsageHistory($className)
        {
            $this->_objectCacheUsageHistory[$className] = $this->_now();
        }

        /**
         * Destroy objects in the cache that have not been used recently
         */
        function _gcObjectCache()
        {
            if ($this->_config->objectCacheTTL < 1) {
                return;
            }

            $cutOff = $this->_now() - $this->_config->objectCacheTTL;

            foreach ($this->_objectCache as $className => $obj) {
                if (!isset($this->_objectCacheUsageHistory[$className]) || $this->_objectCacheUsageHistory[$className] < $cutOff) {
                    $this->_logger->debug(null, 'Destroying cached object instance: %s', $className);

                    if (method_exists($this->_objectCache[$className], '__destruct')) {
                        $this->_objectCache[$className]->__destruct();
                    }

                    unset($this->_objectCache[$className], $this->_objectCacheUsageHistory[$className]);
                }
            }
        }

        /**
         * Enqueue a command to dispatch to the worker process
         *
         * @param string $command
         * @param int $code
         * @param mixed $data
         * @param int $flags
         */
        function _sendResponse($command, $code, $data = null, $flags = WORKER_RESPONSE_FLAG_LAST_ELEMENT)
        {
            $this->_logger->debug(null, 'Adding new response message to write buffer');

            $this->_writeBuffer[] = array($command, serialize(array($command, new WorkerResponse($code, $data, $flags))) . "\n");

            if ($flags & WORKER_RESPONSE_FLAG_LAST_ELEMENT) {
                $this->_processPendingWrite();
            }

        }

        /**
         * Process and store the config object from the parent
         *
         * This method contains a dirty dirty hack to account for PHP4's lack of autoload
         * capability. Please forgive me.
         *
         * @param WorkerConfig $config
         * @access private
         */
        function _initConfig($config)
        {
            if (!is_a($config, 'WorkerConfig')) {
                if (gettype($config) === 'object' && !is_object($config)) {
                    foreach ($config as $name => $value) {
                        if ($name === 'classLoaderFile') {
                            $classLoaderFile = $value;
                        }
                    }
                }

                if (!isset($classLoaderFile)) {
                    $this->_logger->error(null, 'Parent process sent an invalid config object');
                    exit(WORKER_ERR_INVALID_CONFIG_OBJECT);
                }
            } else {
                if (!isset($config->classLoaderFile)) {
                    $this->_logger->error(null, 'Class loader file not defined');
                    exit(WORKER_ERR_CLASS_LOADER_NOT_DEFINED);
                }

                $classLoaderFile = $config->classLoaderFile;
            }

            if (!file_exists($classLoaderFile)) {
                $this->_logger->error(null, 'Class loader path does not exist');
                exit(WORKER_ERR_CLASS_LOADER_NOT_EXIST);
            } else if (!is_file($classLoaderFile)) {
                $this->_logger->error(null, 'Class loader path is not a file');
                exit(WORKER_ERR_CLASS_LOADER_NOT_EXIST);
            } else if (!is_readable($classLoaderFile)) {
                $this->_logger->error(null, 'Class loader path is not readable');
                exit(WORKER_ERR_CLASS_LOADER_NOT_EXIST);
            }

            $this->_logger->debug(null, 'Loading classes from %s', $classLoaderFile);
            /** @noinspection PhpIncludeInspection */
            require_once $classLoaderFile;

            if (!is_object($config)) {
                $config = @unserialize(serialize($config));

                if (!is_a($config, 'WorkerConfig')) {
                    $this->_logger->error(null, 'Parent process sent an invalid config object');
                    exit(WORKER_ERR_INVALID_CONFIG_OBJECT);
                }
            }

            $this->_config = &$config;
        }

        /**
         * Process a job
         *
         * @param WorkerRequest $request
         * @param WorkerResponse $responseData
         * @return int
         */
        function _processJob(&$request, &$responseData)
        {
            if (!is_a($request, 'WorkerRequest')) {
                $this->_logger->error(null, 'Parent process sent a job in an invalid format');
                exit(WORKER_ERR_UNKNOWN_JOB_FORMAT);
            }

            $parts = preg_split('/::|->/', strtolower($request->method), 2);
            if (count($parts) < 2) {
                $this->_logger->warn(null, 'Parent process sent a job with an invalid method spec: %s', $request->method);
                return WORKER_ERR_INVALID_METHOD_SPEC;
            }

            list($className, $methodName) = $parts;
            if (!class_exists($className)) {
                $this->_logger->warn(null, 'Parent process sent a job with an unknown class: %s', $className);
                return WORKER_ERR_UNKNOWN_CLASS;
            }

            $obj = &$this->_getObject($className);
            $method = array(&$obj, $methodName);
            if (!is_callable($method)) {
                $this->_logger->warn(null, 'Parent process sent a job with an unknown method: %s', $request->method);
                return WORKER_ERR_UNKNOWN_METHOD;
            }

            $this->_logger->debug(null, 'Processing job: %s', $request->method);
            $responseData = call_user_func_array($method, $request->args);

            $this->_updateObjectCacheUsageHistory($className);

            $this->_logger->info(null, 'Processed job: %s', $request->method);
            return WORKER_ERR_SUCCESS;
        }

        /**
         * Handle data waiting to be read from the parent process
         */
        function _processPendingRead()
        {
            $data = fread($this->stdIn, 10240);

            $this->_logger->debug(null, 'Read %d bytes from parent process', strlen($data));

            if ($data === false) {
                $this->_logger->error(null, 'Error reading data from parent process');
                exit(WORKER_ERR_READ_FAILURE);
            } else if ($data === '') {
                $this->_logger->error(null, 'Parent process closed input pipe');
                exit(WORKER_ERR_READ_PIPE_CLOSED);
            }

            $this->_readBuffer .= $data;

            while (false !== $pos = strpos($this->_readBuffer, "\n")) {
                $packet = substr($this->_readBuffer, 0, $pos);
                $this->_readBuffer = substr($this->_readBuffer, $pos + 1);

                list($command, $data) = unserialize($packet);

                switch ($command) {
                    case 'JOB':
                        /** @var WorkerRequest $data */
                        $responseData = null;
                        $responseCode = $this->_processJob($data, $responseData);

                        if (is_array($responseData)) {
                            $responseData = array_values($responseData);

                            for ($i = 0, $l = count($responseData) - 1; $i < $l; $i++) {
                                $this->_sendResponse('JOB', $responseCode, $responseData[$i], WORKER_RESPONSE_FLAG_ARRAY_ELEMENT);
                            }

                            $this->_sendResponse('JOB', $responseCode, $responseData[$i], WORKER_RESPONSE_FLAG_ARRAY_ELEMENT | WORKER_RESPONSE_FLAG_LAST_ELEMENT);
                        } else {
                            $this->_sendResponse('JOB', $responseCode, $responseData);
                        }
                        break;

                    case 'INIT':
                        /** @var WorkerConfig $data */
                        $this->_initConfig($data);

                        if ($pid = posix_getpid()) {
                            $this->_logger->debug(null, 'Found process ID: %d', $pid);
                            $this->_sendResponse('INIT', WORKER_ERR_SUCCESS, $pid);
                        } else {
                            $this->_logger->error(null, 'Unable to retrieve process ID');
                            $this->_sendResponse('INIT', WORKER_ERR_GETPID_FAILURE);
                        }
                        break;

                    case 'TERM':
                        $this->_logger->info(null, 'Received a terminate signal');
                        $this->stop();
                        break;

                    default:
                        $this->_logger->error(null, 'Parent process sent a message with an unknown command: %s', $command);
                        exit(WORKER_ERR_UNKNOWN_COMMAND);
                }
            }
        }

        /**
         * Handle data waiting to be written to the parent process
         */
        function _processPendingWrite()
        {
            unset($this->writeStreams[(int) $this->stdOut]);

            $this->_logger->debug(null, 'Processing write cycle');

            while ($this->_writeBuffer) {
                $currentResponse = &$this->_writeBuffer[0];

                $writtenLength = fwrite($this->stdOut, $currentResponse[1]);
                $this->_logger->debug(null, 'Wrote %d bytes', $writtenLength);

                if ($writtenLength === false) {
                    $this->_logger->error(null, 'Error writing data to parent process');
                    exit(WORKER_ERR_WRITE_FAILURE);
                } else if ($writtenLength < strlen($currentResponse[1])) {
                    $currentResponse[1] = (string) substr($currentResponse[1], $writtenLength);
                    $this->writeStreams[(int) $this->stdOut] = $this->stdOut;
                    break;
                } else {
                    $this->_logger->debug(null, 'Current response message write complete');
                    array_shift($this->_writeBuffer);
                }
            }
        }

        /**
         * Main loop
         */
        function run()
        {
            $this->_logger->info(null, 'Process starting');

            $this->running = true;

            while ($this->running) {
                $r = $this->readStreams;
                $w = $this->writeStreams;
                $e = $t = null;

                stream_select($r, $w, $e, $t);

                if ($w) {
                    $this->_processPendingWrite();
                }

                if ($r) {
                    $this->_processPendingRead();
                }

                $this->_gcObjectCache();
            }

            $this->_logger->info(null, 'Process terminated');
        }

        /**
         * Stop the loop
         */
        function stop()
        {
            $this->_logger->debug(null, 'Stopping process');
            $this->running = false;
        }
    }
}
