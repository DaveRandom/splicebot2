<?php

class WorkerParentEndpoint
{
    /**
     * @var WorkerPool
     */
    var $_workerPool;

    /**
     * @var resource
     */
    var $_handle;

    /**
     * @var resource
     * @access private
     */
    var $_stdIn;

    /**
     * @var resource
     * @access private
     */
    var $_stdOut;

    /**
     * @var resource
     * @access private
     */
    var $_stdErr;

    /**
     * @var float
     * @access private
     */
    var $_lastActivity = 0.0;

    /**
     * @var array
     * @access private
     */
    var $_writeBuffer = array();

    /**
     * @var string
     * @access private
     */
    var $_readBuffer = '';

    /**
     * @var array
     * @access private
     */
    var $_responseArray = array();

    /**
     * @var string
     * @access private
     */
    var $_errBuffer = '';

    /**
     * @var AsyncJob
     * @access private
     */
    var $_currentJob;

    /**
     * @var int
     */
    var $_jobsProcessed = 0;

    /**
     * @var int
     */
    var $id;

    /**
     * @var int
     */
    var $pid = -1;

    /**
     * @var bool
     */
    var $_haveReadWatchers = false;

    /**
     * @var bool
     */
    var $_haveWriteWatcher = false;

    /**
     * @var bool
     */
    var $_destroyed = false;

    /** @noinspection PhpInconsistentReturnPointsInspection */
    /**
     * Constructor
     *
     * @param WorkerPool $workerPool
     * @param resource $procHandle
     * @param resource $stdIn
     * @param resource $stdOut
     * @param resource $stdErr
     * @return WorkerParentEndpoint
     */
    function WorkerParentEndpoint(&$workerPool, $procHandle, $stdIn, $stdOut, $stdErr)
    {
        $this->_workerPool = &$workerPool;
        $this->_handle = $procHandle;
        $this->_stdIn = $stdIn;
        $this->_stdOut = $stdOut;
        $this->_stdErr = $stdErr;
        $this->id = (int) $procHandle;

        $this->_sendCommand('INIT', $workerPool->_config);
        $this->addReadWatchers();
    }

    /**
     * Get the current time as a float
     *
     * @return float
     * @access private
     */
    function _now()
    {
        list($usec, $sec) = explode(' ', microtime());
        return ((float)$usec + (float)$sec);
    }

    /**
     * Enqueue a command to dispatch to the worker process
     *
     * @param string $command
     * @param mixed $data
     * @access private
     */
    function _sendCommand($command, $data = null)
    {
        $this->_writeBuffer[] = array($command, serialize(array($command, $data)) . "\n");
        $this->_processPendingWrite();
    }

    /**
     * Mark this worker as idle
     *
     * @access private
     */
    function _setIdle()
    {
        $this->_currentJob = null;
        $this->_lastActivity = $this->_now();
        $this->_workerPool->_setWorkerIdle($this);
    }

    /**
     * Handle data waiting to be read from the worker's STDOUT
     *
     * @access module
     */
    function _processPendingRead()
    {
        if ($this->_destroyed) {
            $this->_workerPool->_logger->debug('WorkerParentEndpoint', 'Refusing to process data for worker #%d', $this->id);
            return;
        }

        $data = fread($this->_stdOut, 10240);

        if ($data === false) {
            $this->_workerPool->_logger->debug('WorkerParentEndpoint', 'Destroy worker #%d from read (data is false)', $this->id);
            $code = $this->_destroy();

            if ($this->_currentJob) {
                $this->_currentJob->_eventManager->trigger('error', 'Unable to retrieve data from worker process, exit code: ' . $code);
            }
        } else if ($data === '') {
            $this->_workerPool->_logger->debug('WorkerParentEndpoint', 'Destroy worker #%d from read (data is empty)', $this->id);
            $code = $this->_destroy();

            if ($this->_currentJob) {
                $this->_currentJob->_eventManager->trigger('error', 'Worker process terminated unexpectedly, exit code: ' . $code);
            }
        } else {
            $this->_readBuffer .= $data;
            $this->_workerPool->_logger->debug('WorkerParentEndpoint', 'Read buffer is %d bytes', strlen($this->_readBuffer));

            while (false !== $pos = strpos($this->_readBuffer, "\n")) {
                $packet = substr($this->_readBuffer, 0, $pos);
                $this->_readBuffer = substr($this->_readBuffer, $pos + 1);

                $start = $this->_now();
                list($command, $response) = unserialize($packet);
                $duration = $this->_now() - $start;
                $this->_workerPool->_logger->debug('WorkerParentEndpoint', 'unserialize() took %s seconds', $duration);

                switch ($command) {
                    case 'JOB':
                        /** @var WorkerResponse $response */
                        if (!is_a($response, 'WorkerResponse')) {
                            $this->_currentJob->_eventManager->trigger('error', 'Worker process returned an invalid response: invalid class');
                            $this->terminate(SIGKILL);
                            break;
                        }

                        if ($response->code !== WORKER_ERR_SUCCESS) {
                            $this->_currentJob->_eventManager->trigger('error', 'Worker process returned error: ' . $response->code);

                            if ($response->code & WORKER_ERR_FATAL) {
                                $this->terminate(SIGKILL);
                                break;
                            }
                        }

                        if ($response->flags & WORKER_RESPONSE_FLAG_ARRAY_ELEMENT) {
                            $this->_responseArray[] = $response->data;

                            if (!($response->flags & WORKER_RESPONSE_FLAG_LAST_ELEMENT)) {
                                break;
                            }

                            $responseData = $this->_responseArray;
                            $this->_responseArray = array();
                        } else if ($this->_responseArray) {
                            $this->_currentJob->_eventManager->trigger('error', 'Worker process returned an invalid response: expecting array element');
                            $this->terminate(SIGKILL);
                            break;
                        } else {
                            $responseData = $response->data;
                        }

                        $this->_currentJob->_eventManager->trigger('complete', $responseData);
                        $this->_jobsProcessed++;
                        $this->_setIdle();
                        break;

                    case 'INIT':
                        /** @var WorkerResponse $response */
                        if (!is_a($response, 'WorkerResponse')) {
                            $this->terminate(SIGKILL);
                        } else {
                            $this->pid = $response->data;
                            $this->_setIdle();
                        }
                        break;

                    default:
                        if ($this->_currentJob) {
                            $this->_currentJob->_eventManager->trigger('error', 'Worker process sent some garbage data');
                        }

                        $this->terminate(SIGKILL);
                        break 2;
                }
            }
        }
    }

    /**
     * Handle data waiting to be read from the worker's STDERR
     *
     * @access module
     */
    function _processPendingErr()
    {
        if ($this->_destroyed) {
            $this->_workerPool->_logger->debug('WorkerParentEndpoint', 'Refusing to process err for worker #%d', $this->id);
            return;
        }

        $data = fread($this->_stdErr, 10240);

        if ($data === false) {
            $this->_workerPool->_logger->debug('WorkerParentEndpoint', 'Destroy worker #%d from err (data is false)', $this->id);
            $code = $this->_destroy();

            if ($this->_currentJob) {
                $this->_currentJob->_eventManager->trigger('error', 'Unable to retrieve data from worker process, exit code: ' . $code);
            }
        } else if ($data === '') {
            $this->_workerPool->_logger->debug('WorkerParentEndpoint', 'Destroy worker #%d from err (data is empty)', $this->id);
            $code = $this->_destroy();

            if ($this->_currentJob) {
                $this->_currentJob->_eventManager->trigger('error', 'Worker process terminated unexpectedly, exit code: ' . $code);
            }
        } else {
            $this->_errBuffer .= $data;

            while (false !== $pos = strpos($this->_errBuffer, "\n")) {
                $packet = substr($this->_errBuffer, 0, $pos + 1);
                $this->_errBuffer = substr($this->_errBuffer, $pos + 1);

                if ($logMessage = @unserialize($packet)) {
                    list($level, $time, $source, $message) = $logMessage;
                    $this->_workerPool->_logger->relay($level, $time, $source, $message, 'worker:' . $this->pid);
                } else {
                    $this->_workerPool->_logger->error('WorkerParentEndpoint', 'Worker #%d: %s', $this->id, trim($packet));
                }
            }
        }
    }

    /**
     * Handle data waiting to be written to the worker process' STDIN
     *
     * @access module
     */
    function _processPendingWrite()
    {
        if ($this->_destroyed || !$this->_writeBuffer) {
            return;
        }

        $currentCommand = &$this->_writeBuffer[0];
        $writtenLength = fwrite($this->_stdIn, $currentCommand[1]);

        if ($writtenLength === false) {
            if ($currentCommand[0] === 'JOB') {
                $this->_workerPool->unshiftJob($this->_currentJob);
            }

            $this->_workerPool->_logger->debug('WorkerParentEndpoint', 'Destroy worker #%d from write (length is false)', $this->id);
            $this->_destroy();
            return;
        }

        if ($writtenLength < strlen($currentCommand[1])) {
            $currentCommand[1] = (string) substr($currentCommand[1], $writtenLength);
        } else {
            if ($currentCommand[0] === 'JOB') {
                $this->_currentJob->_eventManager->trigger('begin');
            }

            array_shift($this->_writeBuffer);
        }

        if ($this->_writeBuffer) {
            $this->addWriteWatcher();
        }
    }

    function _destroy()
    {
        $this->_destroyed = true;
        return $this->_workerPool->_destroyWorker($this);
    }

    /**
     * Get the child's process handle
     *
     * @return resource
     */
    function getHandle()
    {
        return $this->_handle;
    }

    /**
     * Get the child's STDIN stream
     *
     * @return resource
     */
    function getStdIn()
    {
        return $this->_stdIn;
    }

    /**
     * Get the child's STDOUT stream
     *
     * @return resource
     */
    function getStdOut()
    {
        return $this->_stdOut;
    }

    /**
     * Get the child's STDERR stream
     *
     * @return resource
     */
    function getStdErr()
    {
        return $this->_stdErr;
    }

    /**
     * Add read watchers on STDOUT and STDERR in the owner event loop
     */
    function addReadWatchers()
    {
        if (!$this->_haveReadWatchers) {
            // The order of this is significant! ERR is intentionally registered before OUT! DO NOT "FIX" THIS!
            $this->_workerPool->_eventLoop->_addStreamWatcher('read', $this->_stdErr, array(&$this, '_processPendingErr'));
            $this->_workerPool->_eventLoop->_addStreamWatcher('read', $this->_stdOut, array(&$this, '_processPendingRead'));
            $this->_haveReadWatchers = true;
        }
    }

    /**
     * Add write watcher on STDIN in the owner event loop
     */
    function addWriteWatcher()
    {
        if (!$this->_haveWriteWatcher) {
            $this->_workerPool->_eventLoop->_addStreamWatcher('write', $this->_stdIn, array(&$this, '_processPendingWrite'));
            $this->_haveWriteWatcher = true;
        }
    }

    /**
     * Remove read watchers on STDOUT and STDERR in the owner event loop
     */
    function removeReadWatchers()
    {
        if ($this->_haveReadWatchers) {
            $this->_workerPool->_eventLoop->_removeStreamWatcher('read', $this->_stdErr);
            $this->_workerPool->_eventLoop->_removeStreamWatcher('read', $this->_stdOut);
            $this->_haveReadWatchers = false;
        }
    }

    /**
     * Remove write watcher on STDIN in the owner event loop
     */
    function removeWriteWatcher()
    {
        if ($this->_haveWriteWatcher) {
            $this->_workerPool->_eventLoop->_removeStreamWatcher('write', $this->_stdIn);
            $this->_haveWriteWatcher = false;
        }
    }

    /**
     * Get the timestamp of the last activity
     *
     * @return float
     */
    function getLastActivity()
    {
        return $this->_lastActivity;
    }

    /**
     * Get the number of jobs this worker has processed
     *
     * @return int
     * @access private
     */
    function getJobsProcessed()
    {
        return $this->_jobsProcessed;
    }

    /**
     * Dispatch a job to this worker for processing
     *
     * @param AsyncJob $job
     */
    function processJob(&$job)
    {
        $this->_currentJob = &$job;
        $this->_sendCommand('JOB', $job->request);
    }

    /**
     * Determine whether the worker process is still running
     *
     * @return bool
     */
    function isRunning()
    {
        return $this->pid > 0 && posix_kill($this->pid, 0);
    }

    /**
     * Terminate this worker
     *
     * @param int $signal
     */
    function terminate($signal = null)
    {
        $this->addReadWatchers();

        if ($signal === null || $this->pid < 0) {
            $this->_sendCommand('TERM');
        } else {
            posix_kill($this->pid, $signal);
        }
    }
}
