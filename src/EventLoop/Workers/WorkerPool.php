<?php

if (!defined('SIGKILL')) {
    define('SIGKILL', 9);
}
if (!defined('SIGTERM')) {
    define('SIGTERM', 15);
}

class WorkerPool
{
    /**
     * @var EventLoop
     */
    var $_eventLoop;

    /**
     * @var WorkerConfig
     * @access module
     */
    var $_config;

    /**
     * @var EventLoopLogger
     * @access private
     */
    var $_logger;

    /**
     * @var string
     */
    var $name;

    /**
     * @var float
     */
    var $_id;

    /**
     * @var AsyncJob[]
     */
    var $_jobQueue = array();

    /**
     * @var WorkerParentEndpoint[]
     */
    var $_workers = array();

    /**
     * @var int
     */
    var $_workersCount = 0;

    /**
     * @var WorkerParentEndpoint[]
     */
    var $_activeWorkers = array();

    /**
     * @var WorkerParentEndpoint[]
     */
    var $_idleWorkers = array();

    /**
     * @var array
     */
    var $_startingWorkerJobs = array();

    /**
     * @var int[]
     */
    var $_stoppingWorkerTimerIds = array();

    /**
     * @var array
     */
    var $_workerIniSettings = array(
        'error_reporting' => -1,
    );

    /** @noinspection PhpInconsistentReturnPointsInspection */
    /**
     * Constructor
     *
     * @param EventLoop $eventLoop
     * @param WorkerConfig $config
     * @param EventLoopLogger $logger
     * @param string $name
     * @return WorkerPool
     */
    function WorkerPool(&$eventLoop, &$config, &$logger, $name = 'Unnamed')
    {
        $this->_eventLoop = &$eventLoop;
        $this->_config = &$config;
        $this->_logger = &$logger;
        $this->name = $name . ' worker pool';

        // Make sure this object has a unique property for comparisons
        $this->_id = $this->_now();
    }

    /**
     * Get the current time as a float
     *
     * @return float
     * @access private
     */
    function _now()
    {
        list($usec, $sec) = explode(' ', microtime());
        return ((float)$usec + (float)$sec);
    }

    /**
     * Get an idle worker
     *
     * @return WorkerParentEndpoint
     */
    function &_getIdleWorker()
    {
        reset($this->_idleWorkers);
        $worker = &$this->_idleWorkers[key($this->_idleWorkers)];

        return $worker;
    }

    /**
     * Create a new worker process
     *
     * @return WorkerParentEndpoint
     */
    function &_createWorker()
    {
        $cmdArgs = array();
        foreach ($this->_workerIniSettings as $key => $value) {
            $cmdArgs[] = sprintf('-d %s=%s', $key, $value);
        }
        $iniSettings = implode(' ', $cmdArgs);

        $cmd = sprintf('%s %s %s/child.php', $this->_config->phpBinaryPath, $iniSettings, dirname(__FILE__));
        $spec = array(
            0 => array('pipe', 'r'),
            1 => array('pipe', 'w'),
            2 => array('pipe', 'w'),
        );

        $this->_logger->info($this->name, 'Creating worker process: %s', $cmd);

        $procHandle = proc_open($cmd, $spec, $pipes);
        if (!$procHandle) {
            $this->_logger->error($this->name, 'Failed to start worker process!');
            return null;
        }

        list($stdIn, $stdOut, $stdErr) = $pipes;
        stream_set_blocking($stdIn, 0);
        stream_set_blocking($stdOut, 0);
        stream_set_blocking($stdErr, 0);
        $worker = &new WorkerParentEndpoint($this, $procHandle, $stdIn, $stdOut, $stdErr);

        $this->_workersCount++;
        $this->_logger->debug($this->name, 'Started worker #%d, worker processes: %d', $worker->id, $this->_workersCount);

        return $worker;
    }

    /**
     * Destroy a worker process
     *
     * @param WorkerParentEndpoint $worker
     * @return int
     */
    function _destroyWorker(&$worker)
    {
        $workerId = $worker->id;

        $this->_logger->info($this->name, 'Destroying worker process #%d (pid: %d)', $workerId, $worker->pid);

        $worker->removeReadWatchers();
        $worker->removeWriteWatcher();

        fclose($worker->getStdIn());
        fclose($worker->getStdOut());
        fclose($worker->getStdErr());
        $exitCode = proc_close($worker->getHandle());
        unset($worker);

        $this->_logger->debug($this->name, 'Worker process #%d resources destroyed successfully, exit code: %d', $workerId, $exitCode);

        if (isset($this->_stoppingWorkerTimerIds[$workerId])) {
            $this->_eventLoop->cancel($this->_stoppingWorkerTimerIds[$workerId]);
        }

        unset(
            $this->_startingWorkerJobs[$workerId],
            $this->_idleWorkers[$workerId],
            $this->_activeWorkers[$workerId],
            $this->_stoppingWorkerTimerIds[$workerId]
        );

        $this->_workersCount--;
        $this->_logger->debug($this->name, 'Worker processes: %d', $this->_workersCount);

        return $exitCode;
    }

    /**
     * Mark a worker as idle and ready to receive jobs
     *
     * @param WorkerParentEndpoint $worker
     * @access module
     */
    function _setWorkerIdle(&$worker)
    {
        if (isset($this->_startingWorkerJobs[$worker->id])) {
            $this->_dispatchJobToWorker($worker, $this->_startingWorkerJobs[$worker->id][1]);
            unset($this->_startingWorkerJobs[$worker->id]);

            return;
        }

        unset($this->_activeWorkers[$worker->id]);
        $worker->removeReadWatchers();

        if ($this->_config->maxJobsPerProcess > 0 && $worker->getJobsProcessed() >= $this->_config->maxJobsPerProcess) {
            $this->_logger->info(
                $this->name, 'Worker #%d (pid: %d) has processed %d jobs and will be killed',
                $worker->id, $worker->pid, $worker->getJobsProcessed()
            );

            $this->_terminateWorker($worker);

            return;
        }

        $this->_idleWorkers[$worker->id] = &$worker;
        ksort($this->_idleWorkers);

        $this->_logger->debug($this->name, 'Worker #%d (pid: %d) is idle', $worker->id, $worker->pid);
    }

    /**
     * Mark a worker as active and processing a job
     *
     * @param WorkerParentEndpoint $worker
     */
    function _setWorkerActive(&$worker)
    {
        unset($this->_idleWorkers[$worker->id], $this->_startingWorkerJobs[$worker->id]);
        $this->_activeWorkers[$worker->id] = &$worker;

        $worker->addReadWatchers();

        $this->_logger->debug($this->name, 'Worker #%d (pid: %d) is active', $worker->id, $worker->pid);
    }

    /**
     * Dispatch a job to a worker process
     *
     * @param WorkerParentEndpoint $worker
     * @param AsyncJob $job
     */
    function _dispatchJobToWorker(&$worker, &$job)
    {
        $this->_logger->info(
            $this->name, 'Dispatching job to worker #%d (pid: %d), method: %s(%s)',
            $worker->id, $worker->pid, $job->request->method, @implode(', ', $job->request->args)
        );

        $worker->processJob($job);
        $this->_setWorkerActive($worker);
    }

    /**
     * Destroy inactive workers
     */
    function _gcIdleWorkers()
    {
        $cutOff = $this->_now() - $this->_config->maxIdleTime;

        foreach ($this->_idleWorkers as $id => $worker) {
            if ($worker->getLastActivity() < $cutOff) {
                $this->_terminateWorker($this->_idleWorkers[$id]);
                unset($this->_idleWorkers[$id]);
            }
        }
    }

    /**
     * Terminate a worker process
     *
     * @param WorkerParentEndpoint $worker
     * @param int $signal
     */
    function _terminateWorker(&$worker, $signal = null)
    {
        $this->_logger->info(
            $this->name, 'Terminating worker #%d (pid: %d), signal: %s',
            $worker->id, $worker->pid, $signal !== null ? $signal : 'none'
        );

        $worker->terminate($signal);

        if ($signal === null || $signal === SIGTERM) {
            $nextSignal = $signal === null ? SIGTERM : SIGKILL;

            $this->_stoppingWorkerTimerIds[$worker->id] = $this->_eventLoop->in(
                $this->_config->workerTerminateTimeout * 1000000,
                array(&$this, '_terminateWorker'),
                array(&$worker, $nextSignal)
            );
        }
    }

    /**
     * Hand queued async jobs off to worker processes, start new workers and GC old ones
     */
    function processActivity()
    {
        while ($this->_jobQueue && $this->_idleWorkers) {
            $this->_dispatchJobToWorker($this->_getIdleWorker(), $this->shiftJob());
        }

        $workersToStart = min(count($this->_jobQueue), $this->_config->maxWorkers - $this->_workersCount);
        for ($started = 1; $started <= $workersToStart; $started++) {
            $this->_logger->debug($this->name, 'Starting worker %d of %d', $started, $workersToStart);

            if ($worker = &$this->_createWorker()) {
                $job = &$this->shiftJob();
                $this->_startingWorkerJobs[$worker->id] = array(&$worker, &$job);
            }
        }

        $this->_gcIdleWorkers();
    }

    /**
     * Get the name of this worker pool
     *
     * @return string
     */
    function getName()
    {
        return $this->name;
    }

    /**
     * Inspect whether there is work in progress
     *
     * @return bool
     */
    function isActive()
    {
        $this->_logger->debug($this->name, 'Queued jobs %d, Active workers %d', count($this->_jobQueue), count($this->_activeWorkers));

        return $this->_jobQueue || $this->_activeWorkers;
    }

    /**
     * Get the time until there is something to do
     *
     * @return float|null
     */
    function getNextActivityTime()
    {
        if (!$this->_idleWorkers && !$this->_jobQueue) {
            return null;
        } else if ($this->_jobQueue && $this->_workersCount < $this->_config->maxWorkers) {
            return 0;
        }

        $min = null;
        $maxIdleTime = $this->_config->maxIdleTime;

        foreach ($this->_idleWorkers as $worker) {
            $interval = $worker->getLastActivity() + $maxIdleTime - $this->_now();

            if ($min < $interval || $min === null) {
                $min = $interval;
            }
        }

        return max($min, 0);
    }

    /**
     * Add a job at the end of the queue
     *
     * @param AsyncJob $job
     */
    function pushJob(&$job)
    {
        $this->_jobQueue[] = &$job;

        $this->_logger->debug($this->name, 'Job added to end of queue: %s', $job->request->method);
    }

    /**
     * Add a job add the front of the queue
     *
     * @param AsyncJob $job
     */
    function unshiftJob(&$job)
    {
        array_unshift($this->_jobQueue, null);
        $this->_jobQueue[0] = &$job;

        $this->_logger->debug($this->name, 'Job added to front of queue: %s', $job->request->method);
    }

    /**
     * Get the job at the front of the queue
     *
     * @return AsyncJob
     */
    function &shiftJob()
    {
        if ($this->_jobQueue) {
            $job = &$this->_jobQueue[0];
            array_shift($this->_jobQueue);

            $this->_logger->debug($this->name, 'Job removed from front of queue: %s', $job->request->method);
        } else {
            $job = null;

            $this->_logger->debug($this->name, 'No job at the front of the queue to remove');
        }

        return $job;
    }

    /**
     * Terminate all idle worker processes
     *
     * @param int $signal
     */
    function shutdown($signal = null)
    {
        $this->_jobQueue = array();

        while ($this->_startingWorkerJobs) {
            reset($this->_startingWorkerJobs);
            $workerId = key($this->_startingWorkerJobs);

            $this->_terminateWorker($this->_startingWorkerJobs[$workerId][0], $signal);
            unset($this->_startingWorkerJobs[$workerId]);
        }

        foreach ($this->_idleWorkers as $id => $worker) {
            $this->_terminateWorker($this->_idleWorkers[$id], $signal);
        }
    }
}
