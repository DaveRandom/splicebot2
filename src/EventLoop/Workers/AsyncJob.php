<?php

/**
 * Class AsyncJob
 *
 * @event error
 * @event complete
 * @event begin
 */
class AsyncJob
{
    /**
     * @var WorkerRequest
     */
    var $request;

    /**
     * @var EventManager
     */
    var $_eventManager;

    /** @noinspection PhpInconsistentReturnPointsInspection */
    /**
     * Constructor
     *
     * @param string $method
     * @param array $args
     * @return AsyncJob
     */
    function AsyncJob($method, $args)
    {
        $this->request = &new WorkerRequest($method, $args);
        $this->_eventManager = &new EventManager();
    }

    /**
     * Register a handler for an event
     *
     * @param string $eventName
     * @param callable $callback
     * @param array $args
     */
    function on($eventName, $callback, $args = array())
    {
        $this->_eventManager->on($eventName, $callback, $args);
    }

    /**
     * Unregister a handler for an event
     *
     * Returns the number of handlers removed
     *
     * @param string $eventName
     * @param callable $callback
     * @return int
     */
    function off($eventName, $callback = null)
    {
        return $this->_eventManager->off($eventName, $callback);
    }
}
