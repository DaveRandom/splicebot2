<?php

if (!class_exists('WorkerLogger')) {
    class WorkerLogger
    {
        /**
         * Output a log message to the STDERR stream
         *
         * @param string $level
         * @param string $source
         * @param string $message
         */
        function _output($level, $source, $message)
        {
            fwrite(STDERR, serialize(array($level, $this->_now(), $source, $message)) . "\n");
        }

        /**
         * Get the current time as a float
         *
         * @return float
         * @access private
         */
        function _now()
        {
            list($usec, $sec) = explode(' ', microtime());
            return ((float)$usec + (float)$sec);
        }

        /**
         * Log an error-level message
         *
         * @param string $source
         * @param string $message
         * @internal param mixed ...$args
         */
        function error($source, $message)
        {
            if (func_num_args() > 2) {
                $args = func_get_args();
                $message = call_user_func_array('sprintf', array_slice($args, 1));
            }

            $this->_output('error', $source !== null ? $source : 'event loop', $message);
        }

        /**
         * Log a warn-level message
         *
         * @param string $source
         * @param string $message
         * @internal param mixed ...$args
         */
        function warn($source, $message)
        {
            if (func_num_args() > 2) {
                $args = func_get_args();
                $message = call_user_func_array('sprintf', array_slice($args, 1));
            }

            $this->_output('warn', $source !== null ? $source : 'event loop', $message);
        }

        /**
         * Log an info-level message
         *
         * @param string $source
         * @param string $message
         * @internal param mixed ...$args
         */
        function info($source, $message)
        {
            if (func_num_args() > 2) {
                $args = func_get_args();
                $message = call_user_func_array('sprintf', array_slice($args, 1));
            }

            $this->_output('info', $source !== null ? $source : 'event loop', $message);
        }

        /**
         * Log a debug-level message
         *
         * @param string $source
         * @param string $message
         * @internal param mixed ...$args
         */
        function debug($source, $message)
        {
            if (func_num_args() > 2) {
                $args = func_get_args();
                $message = call_user_func_array('sprintf', array_slice($args, 1));
            }

            $backtrace = debug_backtrace();
            $caller =  isset($backtrace[1]['class'])
                ? $backtrace[1]['class'] . $backtrace[1]['type'] . $backtrace[1]['function']
                : $backtrace[1]['function'];

            $this->_output('debug', $source !== null ? $source : 'event loop', $caller . ': ' . $message);
        }
    }
}
