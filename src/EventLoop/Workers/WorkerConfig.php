<?php

if (!class_exists('WorkerConfig')) {
    class WorkerConfig
    {
        /**
         * The maximum number of dynamic worker processes that can be active at one time.
         *
         * @var int
         */
        var $maxWorkers = 5;

        /**
         * How long a worker will be left idle before it is terminated, in seconds.
         *
         * @var int
         */
        var $maxIdleTime = 60;

        /**
         * How many jobs a worker can process before it will be killed.
         *
         * Periodically killing and respawning processes in a busy application can help mitigate issues with
         * memory leaks caused by PHP4's dumb reference counter, and by external libraries. Set to <1 to disable.
         *
         * @var int
         */
        var $maxJobsPerProcess = 500;

        /**
         * How long cached objects will be kept alive in worker processes, in seconds.
         *
         * The object cache keeps instances in memory between jobs to help reduce the amount of CPU time spent on
         * spin up/spin down tasks. They will be destroyed when they have not processed a job after this time.
         *
         * @var int
         */
        var $objectCacheTTL = 60;

        /**
         * The path to the PHP binary used to execute workers.
         *
         * @var string
         */
        var $phpBinaryPath = 'php';

        /**
         * Absolute path to file that loads application classes used in workers
         *
         * @var string
         */
        var $classLoaderFile = null;

        /**
         * How long to wait for a worker to exit gracefully after a terminate command is sent, in seconds.
         *
         * After the timeout has elapsed the process will be sent a SIGKILL signal.
         *
         * @var int
         */
        var $workerTerminateTimeout = 2;

        /**
         * Get an array of inheritable properties and default values
         *
         * @param WorkerConfig $target
         */
        function &normalizeInheritableProperties($target)
        {
            static $inheritableProperties = array(
                'phpBinaryPath' => 'php',
                'workerTerminateTimeout' => 2,
            );

            foreach ($inheritableProperties as $name => $value) {
                if ($this->{$name} === $value && $target->{$name} !== $value) {
                    $this->{$name} = $target->{$name};
                }
            }
        }
    }
}
