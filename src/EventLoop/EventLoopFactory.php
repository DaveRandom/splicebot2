<?php

class EventLoopFactory
{
    /**
     * Create a new EventLoop
     *
     * @param EventLoopConfig $config
     * @param object $logger
     * @return EventLoop
     */
    function &create($config = null, $logger = null)
    {
        if (!isset($config)) {
            $config = &new EventLoopConfig;
        }

        return new EventLoop(
            new WorkerPoolFactory, $config, new EventLoopLogger($logger),
            new SchedulerFactory, new SocketStreamHandler
        );
    }
}
