<?php

class StreamHandler
{
    /** @noinspection PhpInconsistentReturnPointsInspection */
    /**
     * Create a new SocketStreamHandler
     *
     * @param EventLoop $eventLoop
     * @param array $url
     * @return Stream
     * @abstract
     */
    function &createStream(
        /** @noinspection PhpUnusedParameterInspection */ &$eventLoop,
        /** @noinspection PhpUnusedParameterInspection */ $url
    ) {}
}
