<?php

class Stream
{
    /**
     * @var EventManager
     * @access protected
     */
    var $_eventManager;

    /**
     * Constructor
     */
    function Stream()
    {
        $this->_eventManager = &new EventManager();
    }

    /**
     * Register a handler for an event
     *
     * @param string $eventName
     * @param callable $callback
     * @param array $args
     */
    function on($eventName, $callback, $args = array())
    {
        $this->_eventManager->on($eventName, $callback, $args);
    }

    /**
     * Unregister a handler for an event
     *
     * Returns the number of handlers removed
     *
     * @param string $eventName
     * @param callable $callback
     * @return int
     */
    function off($eventName, $callback = null)
    {
        return $this->_eventManager->off($eventName, $callback);
    }

    /**
     * Write some data to the socket
     *
     * @param string $data
     * @abstract
     */
    function write($data) {}

    /**
     * Close the socket
     *
     * @abstract
     */
    function close() {}

    /**
     * Attach another stream to this one
     *
     * @param Stream $stream
     * @param bool $closeOnEof
     * @return bool
     */
    function attach(&$stream, $closeOnEof = true)
    {
        if (!is_a($stream, 'Stream')) {
            return false;
        }

        $stream->on('data', array(&$this, 'write'));
        $this->on('data', array(&$stream, 'write'));

        if ($closeOnEof) {
            $stream->on('close', array(&$this, 'close'));
            $stream->on('error', array(&$this, 'close'));
            $this->on('close', array(&$stream, 'close'));
            $this->on('error', array(&$stream, 'close'));
        }

        $stream->on('close', array(&$this, 'detach'), array(&$stream));
        $stream->on('error', array(&$this, 'detach'), array(&$stream));
        $this->on('close', array(&$stream, 'detach'), array(&$this));
        $this->on('error', array(&$stream, 'detach'), array(&$this));

        return true;
    }

    /**
     * Attach another stream to this one
     *
     * @param Stream $stream
     * @return bool
     */
    function detach(&$stream)
    {
        if (!is_a($stream, 'Stream')) {
            return false;
        }

        $stream->off('data', array(&$this, 'write'));
        $this->off('data', array(&$stream, 'write'));

        $stream->off('close', array(&$this, 'close'));
        $stream->off('close', array(&$this, 'detach'));
        $stream->off('error', array(&$this, 'close'));
        $stream->off('error', array(&$this, 'detach'));
        $this->off('close', array(&$stream, 'close'));
        $this->off('close', array(&$stream, 'detach'));
        $this->off('error', array(&$stream, 'close'));
        $this->off('error', array(&$stream, 'detach'));

        return true;
    }
}
