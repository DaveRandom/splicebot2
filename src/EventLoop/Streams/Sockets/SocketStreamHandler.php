<?php

class SocketStreamHandler extends StreamHandler
{
    /**
     * Create a new client socket
     *
     * @param EventLoop $eventLoop
     * @param array $url
     * @return SocketStream
     */
    function &createStream(&$eventLoop, $url)
    {
        if (!isset($url['host'])) {
            $eventLoop->_logger->warn('Sockets', 'Unable to create socket connection: Missing host');
            return null;
        } else if (!isset($url['port'])) {
            $eventLoop->_logger->warn('Sockets', 'Unable to create socket connection: Missing port');
            return null;
        }

        $eventLoop->_logger->debug(null, 'Creating socket connection to %s://%s:%d', $url['scheme'], $url['host'], $url['port']);
        return new SocketStream($eventLoop, $eventLoop->config->sockets, $eventLoop->_logger, $url['scheme'], $url['host'], $url['port']);
    }
}
