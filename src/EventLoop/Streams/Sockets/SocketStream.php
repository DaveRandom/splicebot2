<?php

class SocketStream extends Stream
{
    /**
     * @var EventLoop
     * @access private
     */
    var $_eventLoop;

    /**
     * @var SocketConfig
     * @access private
     */
    var $_config;

    /**
     * @var EventLoopLogger
     * @access private
     */
    var $_logger;

    /**
     * @var resource
     * @access private
     */
    var $_socket;

    /**
     * @var string
     * @access private
     */
    var $_protocol;

    /**
     * @var string
     * @access private
     */
    var $_host;

    /**
     * @var string
     * @access private
     */
    var $_inetAddr;

    /**
     * @var int
     * @access private
     */
    var $_port;

    /**
     * @var int
     * @access module
     */
    var $_id;

    /**
     * @var string
     * @access private
     */
    var $_writeBuffer = '';

    /**
     * @todo: possibly make this a status integer instead
     * @var bool
     */
    var $_closed = false;

    /** @noinspection PhpInconsistentReturnPointsInspection */
    /**
     * Constructor
     *
     * @param EventLoop $eventLoop
     * @param SocketConfig $config
     * @param EventLoopLogger $logger
     * @param string $protocol
     * @param string $host
     * @param int $port
     * @return SocketStream
     */
    function SocketStream(&$eventLoop, &$config, &$logger, $protocol, $host, $port)
    {
        $this->Stream();

        $this->_eventLoop = &$eventLoop;
        $this->_config = &$config;
        $this->_logger = &$logger;

        $this->_protocol = $protocol;
        $this->_host = $host;
        $this->_port = $port;

        $this->_logger->debug('Sockets', 'Resolving host: %s', $host);
        $this->_eventLoop->resolveHost($host, array(&$this, '_dnsResolutionHandler'));
    }

    /**
     * Handle the DNS lookup result
     *
     * @param string $inetAddr
     * @param null $error
     */
    function _dnsResolutionHandler($inetAddr, $error = null)
    {
        if ($this->_closed) {
            return;
        }

        if ($inetAddr === null) {
            $message = sprintf('Unable to resolve host %s: %s', $inetAddr, $error !== null ? $error : 'Lookup failed');

            $this->_logger->warn('Sockets', $message);
            $this->_error($message);

            return;
        }

        $this->_inetAddr = $inetAddr;

        $target = sprintf('%s://%s', $this->_protocol, $inetAddr);
        $protoHost = sprintf('%s://%s', $this->_protocol, $this->_host);
        $port = $this->_port;

        $this->_logger->debug('Sockets', 'Creating socket connection to %s:%d (%s:%d), timeout %d', $target, $port, $protoHost, $port, $this->_config->connectTimeout);
        $socket = fsockopen($target, $this->_port, $errNo, $errStr, $this->_config->connectTimeout);
        if (!$socket) {
            $this->_logger->warn('Sockets', 'Unable to create socket connection to %s:%d (%s:%d): %d: %s', $target, $port, $protoHost, $port, $errNo, $errStr);
        }

        stream_set_blocking($socket, 0);
        $this->_socket = $socket;
        $this->_id = (int) $socket;

        $this->_eventLoop->_addStreamWatcher('read', $socket, array(&$this, '_processPendingRead'));
        //$this->_socketManager->_eventLoop->_addStreamWatcher('except', $socket, array(&$this, '_processPendingOOB'));

        $this->_logger->info('Sockets', 'Created socket #%d, address: %s:%d (%s:%d)', $this->_id, $target, $port, $protoHost, $port);

        $this->_eventManager->trigger('connect');
    }

    /**
     * Trigger an error event and remove this client from the event loop
     *
     * @param $message
     * @access private
     */
    function _error($message)
    {
        $this->_destroy();
        $this->_eventManager->trigger('error', $message);
    }

    /**
     * Process data waiting to be read from the socket
     *
     * @access module
     */
    function _processPendingRead()
    {
        $data = fread($this->_socket, 10240);

        if ($data === false) {
            $this->_error('Read operation failed');
        } else if ($data === '') {
            $this->close();
        } else {
            $this->_eventManager->trigger('data', $data);
        }
    }

    /**
     * Process out of band data waiting to be read from the socket
     *
     * @access module
     */
    function _processPendingOOB()
    {
        // PHP4 doesn't give use any sane way to handle OOB data, and it's not that gets used much in
        // modern application architectures anyway, so we'll just have to do this until we can use PHP5.
        $this->_error('OOB data received');
    }

    /**
     * Process data waiting to be written to the socket
     *
     * @access module
     */
    function _processPendingWrite()
    {
        if ($this->_writeBuffer !== '') {
            $writtenLength = fwrite($this->_socket, $this->_writeBuffer);

            if ($writtenLength === false) {
                $this->_error('Write operation failed');
            } else if ($writtenLength < strlen($this->_writeBuffer)) {
                $this->_writeBuffer = (string) substr($this->_writeBuffer, $writtenLength);
                $this->_eventLoop->_addStreamWatcher('write', $this->_socket, array(&$this, '_processPendingWrite'));
            } else {
                $this->_writeBuffer = '';
                $this->_eventManager->trigger('drain');
            }
        }
    }

    /**
     * Unregister stream watchers and close socket
     */
    function _destroy()
    {
        $this->_logger->info(null, 'Destroying socket client #%d', $this->_id ? $this->_id : -1);

        if ($this->_socket) {
            $this->_eventLoop->_removeStreamWatcher('read', $this->_socket);
            $this->_eventLoop->_removeStreamWatcher('write', $this->_socket);
            $this->_eventLoop->_removeStreamWatcher('except', $this->_socket);

            @fclose($this->_socket);
            $this->_socket = null;

            $this->_logger->info(null, 'SocketStream #%d was removed', $this->_id);
        }
    }

    /**
     * Write some data to the socket
     *
     * @param string $data
     */
    function write($data)
    {
        $this->_writeBuffer .= $data;
        $this->_processPendingWrite();
    }

    /**
     * Close the socket
     *
     * This *must* be called, rather than simply letting the object go out of scope, or it
     * will cause a memory leak!
     */
    function close()
    {
        $this->_closed = true;
        $this->_destroy();
        $this->_eventManager->trigger('close');
    }
}
