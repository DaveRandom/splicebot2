<?php

class SocketConfig
{
    /**
     * How long to wait for the remote host to respond when attempting to create a client socket connection, in seconds.
     *
     * It is important that this value be kept quite low as socket connects are blocking and will prevent the loop
     * from processing other tasks while the waiting for the connection result.
     *
     * @var int
     */
     var $connectTimeout = 2;
}
