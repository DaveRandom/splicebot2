<?php

define('DNS_LOOKUP_MODE_SINGLE', 1);
define('DNS_LOOKUP_MODE_MULTI',  2);

class DNSConfig extends WorkerConfig
{
    /**
     * The maximum number of dynamic worker processes that can be active at one time.
     *
     * @var int
     */
    var $maxWorkers = 3;

    /**
     * How long a worker will be left idle before it is terminated, in seconds.
     *
     * @var int
     */
    var $maxIdleTime = 30;

    /**
     * How many jobs a worker can process before it will be killed.
     *
     * Not much danger of leaking memory in DNS workers, so set this high
     *
     * @var int
     */
    var $maxJobsPerProcess = 5000;

    /**
     * How long cached objects will be kept alive in worker processes, in seconds.
     *
     * Disable GC in DNS workers since it's static
     *
     * @var int
     */
    var $objectCacheTTL = 0;

    /**
     * Whether to return all records or the first returned record.
     *
     * @var int
     */
    var $lookupMode = DNS_LOOKUP_MODE_SINGLE;

    /**
     * Constructor
     */
    function DNSConfig()
    {
        $this->classLoaderFile = realpath(dirname(__FILE__) . '/class_loader.php');
    }
}
