<?php

class DNSWorker extends WorkerObject
{
    /**
     * Thin wrapper over gethostbyname() which returns null on error
     *
     * @param string $host
     * @return null|string
     */
    function resolve($host)
    {
        $address = gethostbyname($host);
        return $address !== $host ? $address : null;
    }
}
