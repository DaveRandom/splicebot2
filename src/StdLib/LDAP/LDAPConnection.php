<?php

class LDAPConnection
{
    /**
     * @var AsyncObject
     */
    var $_asyncObject;

    /**
     * @var bool
     */
    var $_connected = false;

    /**
     * @var array
     */
    var $_preconnectJobs = array();

    /** @noinspection PhpInconsistentReturnPointsInspection */
    /**
     * Constructor
     *
     * @param EventLoop $eventLoop
     * @param string $url
     * @param string $username
     * @param string $password
     * @return LDAPConnection
     */
    function LDAPConnection(&$eventLoop, $url, $username, $password)
    {
        $config = $eventLoop->config->workers;
        $config->maxWorkers = 1;
        $config->maxJobsPerProcess = 1000;
        $config->classLoaderFile = dirname(__FILE__) . '/class_loader.php';
        $this->_asyncObject = &$eventLoop->createAsyncObject('LDAPWorker', $config);

        $this->_asyncObject->call('connect', array($url, $username, $password), array(&$this, '_onConnect'));
    }

    /**
     * Callback for connection operation
     *
     * @param $result
     */
    function _onConnect($result)
    {
        $this->_connected = (bool) $result;

        if ($this->_connected) {
            foreach ($this->_preconnectJobs as $job) {
                call_user_func_array(array(&$this->_asyncObject, 'call'), $job);
            }

            $this->_preconnectJobs = null;
        }
    }

    /**
     * Call a method asynchronously
     *
     * @param string $method
     * @param array $args
     * @param callable $callback
     */
    function _call($method, $args, $callback)
    {
        if ($this->_connected) {
            $this->_asyncObject->call($method, $args, $callback);
        } else {
            $this->_preconnectJobs[] = array($method, $args, $callback);
        }
    }

    /**
     * Search the directory
     *
     * @param callable $callback
     * @param string $dn
     * @param string $filter
     * @param array $attributes
     */
    function search($callback, $dn, $filter, $attributes = array())
    {
        $this->_call('search', array($dn, $filter, $attributes), $callback);
    }

    /**
     * List the direct children of an object in the directory
     *
     * @param callable $callback
     * @param string $dn
     * @param string $filter
     * @param array $attributes
     */
    function listEntries($callback, $dn, $filter, $attributes = array())
    {
        $this->_call('listEntries', array($dn, $filter, $attributes), $callback);
    }
}
