<?php

class LDAPWorker extends WorkerObject
{
    /**
     * @var resource
     */
    var $directory;

    /**
     * Format a single of result entry into a sane format
     *
     * By-ref arg keeps memory usage to a minimum (avoids COW) but may revisit this API choice
     *
     * @param array $entry
     * @return array
     */
    function _formatEntry(&$entry)
    {
        $l = $entry['count'];
        unset($entry['count']);

        for ($i = 0; $i < $l; $i++) {
            if ($entry[$entry[$i]]['count'] > 1) {
                unset($entry[$entry[$i]]['count']);
            } else {
                $entry[$entry[$i]] = $entry[$entry[$i]][0];
            }

            unset($entry[$i]);
        }

        return $entry;
    }

    /**
     * Format a set of result entries into a sane format
     *
     * By-ref arg keeps memory usage to a minimum (avoids COW) but may revisit this API choice
     *
     * @param array $entries
     * @return array
     */
    function _formatEntries(&$entries)
    {
        $l = $entries['count'];
        unset($entries['count']);

        for ($i = 0; $i < $l; $i++) {
            $this->_formatEntry($entries[$i]);
        }

        return $entries;
    }

    /**
     * Connect to the directory service
     *
     * @param string $url
     * @param string $user
     * @param string $pass
     * @return bool
     */
    function connect($url, $user, $pass)
    {
        if (!$directory = ldap_connect($url)) {
            $this->logger->debug('LDAPWorker', 'Connection failed: connect operation failed');
            return false;
        }

        if (!ldap_bind($directory, $user, $pass)) {
            $this->logger->debug(
                'LDAPWorker', 'Connection failed: bind operation failed: %d: %s',
                ldap_errno($directory), ldap_error($directory)
            );
            return false;
        }

        $this->directory = $directory;
        return true;
    }

    /**
     * Search the directory
     *
     * @param string $dn
     * @param string $filter
     * @param array $attributes
     * @return array|bool
     */
    function search($dn, $filter, $attributes)
    {
        if (!$result = ldap_search($this->directory, $dn, $filter, $attributes)) {
            $this->logger->debug(
                'LDAPWorker', 'Search failed: search operation failed: %d: %s',
                ldap_errno($this->directory), ldap_error($this->directory)
            );
            return false;
        }

        $entries = ldap_get_entries($this->directory, $result);
        ldap_free_result($result);

        if (!$entries) {
            $this->logger->debug(
                'LDAPWorker', 'Search failed: fetching entries failed: %d: %s',
                ldap_errno($this->directory), ldap_error($this->directory)
            );
            return false;
        }

        return $this->_formatEntries($entries);
    }

    /**
     * List the direct children of an object in the directory
     *
     * @param string $dn
     * @param string $filter
     * @param array $attributes
     * @return array|bool
     */
    function listEntries($dn, $filter, $attributes)
    {
        if (!$result = ldap_list($this->directory, $dn, $filter, $attributes)) {
            $this->logger->debug(
                'LDAPWorker', 'Search failed: search operation failed: %d: %s',
                ldap_errno($this->directory), ldap_error($this->directory)
            );
            return false;
        }

        $entries = ldap_get_entries($this->directory, $result);
        ldap_free_result($result);

        if (!$entries) {
            $this->logger->debug(
                'LDAPWorker', 'Search failed: fetching entries failed: %d: %s',
                ldap_errno($this->directory), ldap_error($this->directory)
            );
            return false;
        }

        return $this->_formatEntries($entries);
    }
}
